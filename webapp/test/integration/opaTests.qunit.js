/* global QUnit */
QUnit.config.autostart = false;

sap.ui.getCore().attachInit(function () {
	"use strict";

	sap.ui.require([
		"istn/ci/edu/kmap/JMT/test/integration/AllJourneys"
	], function () {
		QUnit.start();
	});
});