sap.ui.define([
	"sap/ui/core/UIComponent",
	"sap/ui/Device",
	"istn/ci/edu/kmap/JMT/model/models",
	"sap/ui/model/json/JSONModel"
], function (UIComponent, Device, models, JSONModel) {
	"use strict";

	return UIComponent.extend("istn.ci.edu.kmap.JMT.Component", {

		metadata: {
			manifest: "json"
		},

		/**
		 * The component is initialized by UI5 automatically during the startup of the app and calls the init method once.
		 * @public
		 * @override
		 */
		init: function () {
			// call the base component's init function
			UIComponent.prototype.init.apply(this, arguments);

			// enable routing
			this.getRouter().initialize();

			// set the device model
			this.setModel(models.createDeviceModel(), "device");
		},
		
		_initializateFirebase: function (){
			var firebaseConfig = {
			    apiKey: "AIzaSyD8xwqTlLPl_rs_yVE0HLmTM_CDhc3XugY",
			    authDomain: "jmt-project-52a01.firebaseapp.com",
			    databaseURL: "https://jmt-project-52a01.firebaseio.com",
			    projectId: "jmt-project-52a01",
			    storageBucket: "jmt-project-52a01.appspot.com",
			    messagingSenderId: "78330264640",
			    appId: "1:78330264640:web:11b0cd362245687993b3a5"
			  };
			  // Initialize Firebase
			  firebase.initializeApp(firebaseConfig);
			  if(!this._storage){
				  this._storage = firebase.storage();
				  this._storageRef = this._storage.ref();
			  }
		}
	});
});