sap.ui.define([
	"./BaseController",
	"sap/ui/model/json/JSONModel",
	"sap/m/MessageBox",
	'sap/ui/core/Fragment',
	"sap/m/MessageToast"
], function (BaseController, JSONModel, MessageBox, Fragment, MessageToast) {
	"use strict";

	return BaseController.extend("istn.ci.edu.kmap.JMT.controller.Create", {
		onInit: function () {
			this.getRouter().getRoute("Create").attachMatched(this._onRouteMatchedCreate, this);
			this._initModels();
			this._getCodeList();                      
		},
		
		_initModels: function(){
			this.getView().setModel(new JSONModel(), "createData");
			this.i18n = this.getOwnerComponent().getModel("i18n").getResourceBundle();
		},
		
		_onRouteMatchedCreate: function(){
			this._getMap(this.getView().byId("song_map").getId(), "");
			this.getView().getModel("createData").setData({jmtPrice: 10000, jmtRating: 3});
			
			// this._getJmtCategory(); // 기존 카테고리, 지역 데이터가져오기
			this.byId("saveBtn").setEnabled(false);
			this.byId("jmtDate").setDateValue(new Date());
			var center = new kakao.maps.LatLng(37.484177185974836, 127.11674377596071);
			this.map.panTo(center);//회사 좌표로 기본셋팅
			this.map.setZoomable(false);
			this.map.setDraggable(false);
		},
		
		jmtInputChange: function(Event){
			var oSource = Event.getSource();
			if(!Event.getParameter("value") || (Event.getParameter("value").replace(/ /gi, "").length < 2 )){
				oSource.setValueState("Error");
				oSource.setValueStateText(this.i18n.getText("keywordValidator"));
				this.byId("saveBtn").setEnabled(false);
				return;	
			}else{
				oSource.setValueState("None");
			}
		},
		
		onSearchJmt: function (Event) {
			var createData = this.getView().getModel("createData").getData();
			if(this.byId("jmtName").getValue().replace(/ /gi, "").length < 2){
				this.byId("jmtName").setValueState("Error");
				this.byId("saveBtn").setEnabled(false);
				MessageBox.error(this.i18n.getText("keywordValidator"));
				return;
			}else{
				this.byId("jmtName").setValueState("None");
				var data = { "placeName" : createData.jmtName};
				this._openDialog(data);
				this.byId("saveBtn").setEnabled(true);
			}
		},
		
		submitKeyword: function(Event){
			var oSource = Event.getSource();
			if(Event.getParameter("value").replace(/ /gi, "").length < 2){
				oSource.setValueState("Error");
				oSource.setValueStateText(this.i18n.getText("keywordValidator"));
				this.byId("saveBtn").setEnabled(false);
				return;	
			}else{
				oSource.setValueState("None");
				var data = { "placeName" : Event.getParameter("value")};
				this._openDialog(data);
				this.byId("saveBtn").setEnabled(true);
			}
		},
		
		onChangeDate: function(Event){
			var source = Event.getSource();
			if(!Event.getParameter("value")){
				source.setValueState("Error");
				source.setValueStateText(this.i18n.getText("mandatoryField"));
			}else if(!Event.getParameter("valid")){
				source.setValueState("Error");
				source.setValueStateText(this.i18n.getText("dateValidator"));
			}else{
				source.setValueState("None");
			}
		},
		
		onChangeSelect: function(Event){
			var getId = Event.getSource().getId().split("-").reverse()[0];
			var getText = Event.getSource().getSelectedItem().getText();
			var createData = this.getView().getModel("createData").getData();
			if(getId === "jmtLocation"){
				createData["jmtLocation"] = getText;
			}else{
				createData["jmtCategory"] = getText;
			}
		},
		
		onHandelImgType: function(Event){
			MessageBox.error(this.i18n.getText("uploadFileType"));
		},
		
		onAttachFile: function(Event){
			var fileObject = Event.getParameter("files")[0];
			if(fileObject){
				if(fileObject.size > 4194304) return MessageBox.warning(this.i18n.getText("uploadFileSize"));
				var fileModel = this.getView().getModel("createData");
				var files = fileModel.getData();
				if(files.list === undefined){
					files.list = [];
				}else if (files.list.length >= 3){
					return MessageBox.error(this.i18n.getText("uploadFileCount"));
				}
				files.list.push(fileObject);
				fileModel.refresh();
				Event.getSource().setValue("");
			}
		},
		
		onDeleteFile: function(Event){
			var listIdx = Event.getParameter("listItem").getBindingContextPath().split("/").reverse()[0];
			var fileModel = this.getView().getModel("createData");
			var files = fileModel.getData().list;
			files.splice(listIdx, 1);
			fileModel.refresh();
			if(files.length <= 0){ this.byId("fileUploader").setValue(); }
		},
		
		handleUploadStart: function(){
			var createData = this.getView().getModel("createData").getData();
			var fileList = createData.list;
			var form = new FormData();
			for(var i in fileList){
				form.append("myJMT", fileList[i], fileList[i].name);
			}
			var that = this;
			this.byId("createPage").setBusy(true);
			$.ajax({
				url : "/MapService/api/v1/upload",
				method : "POST",
				processData : false,
				contentType : false,
				mimeType : "multipart/form-data",
				data : form,
				timeout: 15000,	//15초 타임아웃
				success: function(data, headers){
					that.byId("createPage").setBusy(false);
					if(data.status === "error") return MessageBox.error(that.i18n.getText("notAvailableService"));
					createData.list = JSON.parse(data);
					that._addJmtData(createData);
				},
				error: function(error){
					that.byId("createPage").setBusy(false);
					if(error.statusText === "timeout") return MessageBox.error(that.i18n.getText("timeOut"));
					return MessageBox.error(that.i18n.getText("notAvailableService"));
				}
			});
		},
		
		onSave: function(){
			var createData = this.getView().getModel("createData").getData();
			if(!createData.jmtName || !createData.jmtAddress || !createData.jmtDate || !createData.jmtPrice || !createData.jmtRating ){
				return MessageBox.error(this.i18n.getText("mandatoryFields"));
			}else{
 				createData.map = {
					x: this.pressedItem.map.x,
					y: this.pressedItem.map.y
				};
				if(createData.list !== undefined){
					this.handleUploadStart();
				}else{
					this._addJmtData(createData);
				}
			}
		},
		
		onCancel: function(){
			var that = this;
			var createData = this.getView().getModel("createData").getData();
			if(createData.jmtName !== undefined || createData.jmtAddress !== undefined || createData.jmtLocation !== undefined ||
			createData.jmtPracie !== undefined || createData.jmtCategory !== undefined || createData.jmtRefer !== undefined ||
			createData.list !== undefined){
				MessageBox.warning(this.i18n.getText("cancelEdit"), {
					actions: [MessageBox.Action.OK, MessageBox.Action.CANCEL],
					emphasizedAction: MessageBox.Action.OK,
					onClose: function (sAction) {
						if(sAction === "OK"){
							that.getView().getModel("createData").setData({});
							that.getRouter().navTo("Main");
						}
					}
				});
			}else{
				that.getRouter().navTo("Main");
			}
		},

		onPressListItemFragment: function(Event){
			var ListIdx = Event.getSource().getBindingContextPath().split("/").reverse()[0];
			this.byId("listItem").setSelectedItem(this.byId("listItem").getItems()[ListIdx]);
			this.pressedItem = this.getView().getModel("dialogData").getData().mapData[ListIdx];
			this._getDialogMap("", this.pressedItem);
		},
		
		onOkFragment: function(){
			var createData = this.getView().getModel("createData").getData();
			createData.jmtName = this.pressedItem.placeName; 
			this.byId("jmtAddress").setValue(this.pressedItem.address);
			this._getMap("", this.pressedItem);
			this.byId("searchLoc").destroy();
			this.getView().getModel("dialogData").setData({});
		},
		
		onCancelFragment: function(){
			this.byId("jmtAddress").setValue();
			this.byId("searchLoc").destroy();
			this.getView().getModel("dialogData").setData({});
		},		
		
		_openDialog: function(data){
			var oView = this.getView();
			if (!this.byId("searchLoc")) {
				Fragment.load({
					id: oView.getId(),
					name: "istn.ci.edu.kmap.JMT.view.SearchJmt",
					controller: this
				}).then(function(oDialog){
					oView.addDependent(oDialog);
					this._getDialogData(data);
					oDialog.open();
				}.bind(this));
			} else {
				this.byId("searchLoc").open();
			}
		},
		
		_getDialogMap: function(firstItem, pressedItem){
			if(!pressedItem){
		        var container = document.getElementById(this.byId("dialogMap").getId()); //지도를 담을 HBox의 html Id
		        var options = { //지도 생성할 때 옵션
		            center: new kakao.maps.LatLng(firstItem.map.y, firstItem.map.x), //지도의 중심좌표.-기본: 회사
		            level: 3 //지도의 레벨(확대, 축소 정도)
		        };
		    	this.dialogMap = new kakao.maps.Map(container, options);
		    	this.dialogMap.setDraggable(false);
		    	
				this.marker = new kakao.maps.Marker({
				    position: options.center, //마크가 표시될 좌표 변수
					clickable: false
				});
				this.marker.setMap(this.dialogMap);
				this.dialogMap.setZoomable(false);
		    	
			}else{
			    // 이동할 위도 경도 위치 setting Ga: 경도(x), Ha: 위도(y)
				var markerPosition = new kakao.maps.LatLng(pressedItem.map.y, pressedItem.map.x)
				
				this.dialogMap.panTo(markerPosition);	// 선택된 아이템의 좌표 정보 기반으로 지도의 중심이동
				
				this.marker.setPosition(markerPosition); //마크가 표시될 좌표 변수
				this.marker.setMap(this.dialogMap);
			}
		},
		
		_getDialogData: function(inputData){
			// get keyword search list
			this.byId("listItem").setBusy(true);
			var that = this;
			$.ajax({
				url: "/MapService/api/v1/place",
				method: "POST",
				headers: {
					"Content-Type": "application/json;charset=utf-8"
				},
				data: JSON.stringify(inputData),
				success : function (data, headers){
					//모델 중복 만들기 안됨
					if(!that.getView().getModel("dialogData")){
						that.getView().setModel(new JSONModel(data.data), "dialogData");
					}else{
						that.getView().getModel("dialogData").setData(data.data);
					}
					//데이터 없는경우
					if(data.data === undefined){
						that.byId("listItem").setBusy(false);
						return MessageBox.error(that.i18n.getText("noKeywordList"), {
							actions: [MessageBox.Action.OK],
							onClose: function (sAction) {
								if(sAction === "OK"){
									that.byId("searchLoc").destroy();
								}
							}	
						});
					}else{
						//dialog map에서 첫 아이템의 위치 보여주기
						var firstItem = that.getView().getModel("dialogData").getData().mapData[0];
						that._getDialogMap(firstItem, "");
						
						//유저가 첫 리스트로 저장할경우에 필요
						that.pressedItem = firstItem;
						
						//dialog list의 첫 아이템 selected
						var listItem = that.byId("listItem");
						listItem.setSelectedItem(listItem.getItems()[0]);
						
						listItem.setBusy(false);
					}
				},
				error : function (error){
					that.byId("listItem").setBusy(false);
					MessageBox.warning(that.i18n.getText("failGetKeywordList"), {
							actions: [MessageBox.Action.OK],
							onClose: function (sAction) {
								if(sAction === "OK"){
									that.byId("searchLoc").destroy();
								}
							}	
						});
				}
			});
		},
		
		_addJmtData: function(jmtData){
			var that = this;
			this.byId("createPage").setBusy(true);
			$.ajax({
				url: "/MapService/api/v1/jmt",
				method: "POST",
				headers: {
					"Content-Type": "application/json;charset=utf-8"
				},
				data: JSON.stringify(jmtData),
				success: function(data, headers){
					MessageBox.success(that.i18n.getText("successSave"), {
							actions: [MessageBox.Action.OK],
							emphasizedAction: MessageBox.Action.OK,
							onClose: function (oAction) {
								that.getOwnerComponent().reload = true;
								that.getRouter().navTo("Main");
							}
						}
					);
					that.byId("createPage").setBusy(false);
				},
				error: function(error){
					that.byId("createPage").setBusy(false);
					return MessageBox.error(error.responseText);
				}
			});
		},
		
		// _getJmtCategory: function(){
		// 	var that = this;
		// 	$.ajax({
		// 		url: "/MapService/api/v1/jmt",
		// 		method: "GET",
		// 		success : function(data, headers){
		// 			if(!that.getView().getModel("jmtList")){
		// 				that.getView().setModel(new JSONModel(data.data), "jmtList");
		// 			}else{
		// 				that.getView().getModel("jmtList").setData(data.data);
		// 			}
		// 		},
		// 		error: function(error){
		// 			MessageBox.warning("기존 카테고리, 지역 목록을 읽어오지 못했어요.");
		// 		}
		// 	});
		// }
		
	});
});