sap.ui.define([
	"./BaseController",
	"sap/m/MessageBox",
	"sap/m/MessageToast",
	"sap/ui/model/json/JSONModel"
], function (BaseController, MessageBox, MessageToast, JSONModel) {
	"use strict";

	return BaseController.extend("istn.ci.edu.kmap.JMT.controller.Display", {

		onInit: function () {
			this.getRouter().getRoute("Display").attachMatched(this._onRouteMatchedDisplay, this);
			this.getOwnerComponent()._initializateFirebase();
			this.getView().setModel(new JSONModel({editBtn:true, footer:false, editable:false}), "setting");
			this.i18n = this.getOwnerComponent().getModel("i18n").getResourceBundle();
			this._getCodeList();
		},
		
		_onRouteMatchedDisplay: function(Event){
			//초기 Display모드로 셋팅
			this.getView().getModel("setting").setData({editBtn:true, footer:false, editable:false});
			this.byId("fileUploader").setValue();
			
			var pressedItem = this.getOwnerComponent().jmtList;
			if(!this.getView().getModel("pressItem")){
				this.getView().setModel(new JSONModel(pressedItem), "pressItem");
			}else{
				this.getView().getModel("pressItem").setData(pressedItem);
			}
			this.originDataDisplay = JSON.parse(JSON.stringify(pressedItem));	//원본데이터
			this._getDisplayMap(pressedItem);	//지도 셋팅
		},
		
		onNavBack: function () {
			var that = this;
			var jmtDataModel = this.getView().getModel("pressItem");
			// var jmtData = jmtDataModel.getData();
			if(JSON.stringify(this.originDataDisplay) === JSON.stringify(jmtDataModel.getData())){
				this.getRouter().navTo("Main");	
			}else{
				MessageBox.warning(this.i18n.getText("cancelEdit"), {
						actions: [MessageBox.Action.OK, MessageBox.Action.CANCEL],
						emphasizedAction: MessageBox.Action.OK,
						onClose: function (oAction) {
							if(oAction === "OK"){
								jmtDataModel.setData(JSON.parse(JSON.stringify((that.originDataDisplay))));
								that.getRouter().navTo("Main");	
							}
						}
					}
				);
			}

		},
		
		// downloadFile_old: function(event){
		// 	// debugger;
			
		// 	// var data = { "fileName" : event.getSource().getTitle() }; 
		// 	var fileData = { "fileName" : "sal9.png" }; 
		// 	$.ajax({
		// 		url : "/MapService/api/v1/download",
		// 		method : "POST",
		// 		headers : {
		// 			"content-type": "application/json;charset=utf-8"
		// 		},
		// 		data : JSON.stringify(fileData),
		// 		success: function(data, headers){
		// 			debugger;
		// 			var reader = new FileReader();
		// 		    reader.onload = function(){
		// 		        var i, l, d, array;
		// 		        d = data;
		// 		        l = d.length;
		// 		        array = new Uint8Array(l);
		// 		        for (i = 0; i < l; i++){
		// 		            array[i] = d.charCodeAt(i);
		// 		        }
		// 		        var b = new Blob([array], {type: 'application/octet-stream'});
		// 		        window.location.href = URL.createObjectURL(b);
		// 		    };
		// 		    debugger;
		// 		    // reader.readAsBinaryString(data);
						
		// 		},
		// 		error: function(error){
		// 			debugger;
		// 		}
		// 	})
			
		// },
		
		downloadFile: function(event){
			var that = this;
			var jmtData = this.getView().getModel("pressItem").getData();
			try{
				var bindingPath = event.getSource().getBindingContextPath().split('/').reverse()[0], that = this;
				var fileName = jmtData.list[bindingPath].url.split("/").reverse()[0];
				var downloadFileName = jmtData.list[bindingPath].name;
				if(!fileName) return MessageBox.error(this.i18n.getText("noFileName"));
				this.downloadFileName = downloadFileName;
			}catch(error){
				MessageBox.error(error)
			}
				
			this.getOwnerComponent()._storageRef.child(fileName).getDownloadURL()
				.then(function(url) {
					MessageToast.show(that.i18n.getText("startFileDownload"));
					that._downLocalSystem(url);
				})
				.catch(function(error) {
					MessageBox.error(that.i18n.getText("failToDownloadFile"));
				});
		
		    /*
		    // 은송 소스
		    debugger;
		    var fileName = Event.getSource().getTitle();
			this.getOwnerComponent()._storageRef.child(fileName).getDownloadURL()
			.then(function(url) {
			// `url` is the download URL for 'images/stars.jpg'
			// This can be downloaded directly:
			var xhr = new XMLHttpRequest();
			xhr.responseType = 'blob';
			xhr.onload = function(url) {
				var blob = xhr.response;
				// var a = document.getElementById('a');
				// var downloadUrl = URL.createObjectURL(blob);
				// a.href = downloadUrl;
				// a.target = "_blank";
				// a.download = fileName;
			 //   debugger;
				// // Or inserted into an <img> element:

				// document.body.appendChild(a);
				// a.click();
				// document.body.removeChild(a);
			  };
			  xhr.open('GET', url);
			  xhr.send();
			
			    debugger;
			}).catch(function(error) {
				debugger;
			  // Handle any errors
			});
			*/
			
		},
		
		_downLocalSystem: function(url){
			var xhr = new XMLHttpRequest(), that = this;
			xhr.responseType = 'blob';
			xhr.onload = function(event) {
				var blob = xhr.response;
				var download = document.createElement('a');
				download.href = URL.createObjectURL(blob, { type: blob.type });
				download.download = `${that.downloadFileName}`; 
				download.target = "_blank";
				document.body.appendChild(download);
				download.click();
				document.body.removeChild(download);
			};
			xhr.onerror = function(error){
				MessageBox.error(that.i18n.getText("failToDownloadFile"));
			}
			xhr.open('GET', url);
			// debugger;
			xhr.send();
		},

		onEdit: function(){
			this._changeEditMode(true);
		},
		
		onHandelImgType: function(Event){
			MessageBox.error(this.i18n.getText("failToDownloadFile"));
		},
		
		onAttachFile: function(Event){
			var fileObject = Event.getParameter("files")[0];
			if(fileObject){
				if(fileObject.size > 4194304) return MessageBox.warning(this.i18n.getText("uploadFileSize"));
				var fileModel = this.getView().getModel("pressItem");
				var files = fileModel.getData();
				if(files.list === undefined){
					files.list = [];
				}else if(files.list.length >= 3){
					this.byId("fileUploader").setValue();
					return MessageBox.error(this.i18n.getText("uploadFileCount"));
				}
				files.list.push(fileObject);
				fileModel.refresh();
				Event.getSource().setValue();
			}
		},
		
		onDeleteFileList: function(Event){
			var listIdx = Event.getParameter("listItem").getBindingContextPath().split("/").reverse()[0];
			var jmtDataModel = this.getView().getModel("pressItem");
			var fileList = jmtDataModel.getData().list;
			if(!jmtDataModel.getData()["deleteFileList"]){
				jmtDataModel.getData()["deleteFileList"] = [fileList[listIdx]];
			}else{
				jmtDataModel.getData()["deleteFileList"].push(fileList[listIdx]);
			}
			fileList.splice(listIdx, 1);
			jmtDataModel.refresh();
			if(fileList.length === 0){ fileList = []; }
		},
		
		onDelete: function(){
			var that = this;
			MessageBox.warning(this.i18n.getText("deleteJmt"), {
					actions: [MessageBox.Action.OK, MessageBox.Action.CANCEL],
					emphasizedAction: MessageBox.Action.OK,
					onClose: function (oAction) { 
						if(oAction === "OK"){
							that._deleteJmtData(that.originDataDisplay.id);
						}
					}
				}
			);
		},
		
		onChangeDate: function(Event){
			var source = Event.getSource();
			if(!Event.getParameter("value")){
				source.setValueState("Error");
				source.setValueStateText(this.i18n.getText("mandatoryField"));
			}else if(!Event.getParameter("valid")){
				source.setValueState("Error");
				source.setValueStateText(this.i18n.getText("dateValidator"));
			}else{
				source.setValueState("None");
			}
		},
		
		onChangeSelect: function(Event){
			var getId = Event.getSource().getId().split("-").reverse()[0];
			var getText = Event.getSource().getSelectedItem().getText();
			var jmtData = this.getView().getModel("pressItem").getData();
			if(getId === "jmtLocation"){
				jmtData["jmtLocation"] = getText;
			}else{
				jmtData["jmtCategory"] = getText;
			}
		},
		
		
		onSave: function(){
			var that = this;
			var jmtData = this.getView().getModel("pressItem").getData();
			if(!jmtData.jmtDate) return MessageBox.warning(this.i18n.getText("mandatoryField"));
			if(this.byId("jmtDate").getValueState()==="Error") return MessageBox.warning(this.i18n.getText("dateValidator"));
			if(JSON.stringify(this.originDataDisplay) === JSON.stringify(jmtData)){
				return MessageBox.warning(this.i18n.getText("noChangedData"));
			}else{
				MessageBox.confirm(this.i18n.getText("saveJmt"), {
						actions: [MessageBox.Action.CANCEL, MessageBox.Action.OK],
						emphasizedAction: MessageBox.Action.OK,
						onClose: function (oAction) { 
							if(oAction === "OK"){
								!jmtData.list || ( jmtData.list.length !== 0 ) ? that.handleUploadStart() : that._updateJmtData(jmtData);
							}
						}
					}
				);
			}
		},
		
		handleUploadStart: function(){
			this.byId("detail").setBusy(true);
			var jmtData = this.getView().getModel("pressItem").getData();
			var form = new FormData();
			for(var i in jmtData.list){
				if(jmtData.list[i] instanceof Blob){
					form.append("myJMT", jmtData.list[i], jmtData.list[i].name);
				}
			}
			var newFiles = form.getAll("myJMT").length;
			if(newFiles > 0){
				var that = this;
				$.ajax({
					url : "/MapService/api/v1/upload",
					method : "POST",
					processData : false,
					contentType : false,
					mimeType : "multipart/form-data",
					data : form,
					timeout: 15000,	//15초 타임아웃
					success: function(data, headers){
						that.byId("detail").setBusy(false);
						if(data.status === "error") return MessageBox.error(that.i18n.getText("notAvailableService"));
						jmtData.list.splice(jmtData.list.length-newFiles, newFiles);
						for(var j=0; j < newFiles; j++){
							var parsedData = JSON.parse(data)[j];
							jmtData.list.push(parsedData);
						}
						that._updateJmtData(jmtData);
					},
					error: function(error){
						that.byId("detail").setBusy(false);
						if(error.statusText === "timeout") return MessageBox.error(that.i18n.getText("timeOut"));
						return MessageBox.error(that.i18n.getText("notAvailableService"));
					}
				});
			}else{
				this._updateJmtData(jmtData);
			}
		},
		
		onCancel: function(){
			var that = this;
			var jmtDataModel = this.getView().getModel("pressItem");
			if(JSON.stringify(this.originDataDisplay) === JSON.stringify(jmtDataModel.getData())){
				this._changeEditMode(false);
			}else{
				MessageBox.warning(this.i18n.getText("cancelEdit"), {
						actions: [MessageBox.Action.OK, MessageBox.Action.CANCEL],
						emphasizedAction: MessageBox.Action.OK,
						onClose: function (oAction) { 
							if(oAction === "OK"){
								that._cancelEdit();
							}
						}
					}
				);
			}
		},
		
		_getDisplayMap: function(pressedItem){
	        var container = document.getElementById(this.getView().byId("song_map").getId()); //지도를 담을 HBox의 html Id
	        var options = { //지도를 생성할 때 필요한 기본 옵션
	            center: new kakao.maps.LatLng(pressedItem.map.y, pressedItem.map.x), //지도의 중심좌표.-기본: 회사
	            level: 3 //지도의 레벨(확대, 축소 정도)
	        };
			
	    	var map = new kakao.maps.Map(container, options); //지도 생성 및 객체 리턴 (컨트롤러 property로 선언)
			map.setDraggable(false);
			var marker = new kakao.maps.Marker({
			    position: options.center, //마크가 표시될 좌표 변수
				clickable: false
			});
			marker.setMap(map);
			map.setZoomable(false);
		},
		
		_changeEditMode: function(mode){
			this.getView().getModel("setting").setData({
				editBtn : !mode, 
				footer : mode, 
				editable : mode,
				listMode : mode ? "Delete" : "None"
			});
			
		},
		
		_cancelEdit: function(){
			var originData = JSON.parse(JSON.stringify(this.originDataDisplay));
			this.getView().getModel("pressItem").setData(originData);
			this._changeEditMode(false);
		},
		
		_deleteFile: function(mode){
			if(mode === "updateDoc"){
				var deleteData = this.getView().getModel("pressItem").getData()["deleteFileList"];
				if(deleteData){
					deleteData.map(item=>{
						var fileName = item.url.split("/").reverse()[0];
						var desertRef = this.getOwnerComponent()._storageRef.child(fileName);
						
						desertRef.delete().then(function() {
						}).catch(function(error) {
						  return MessageBox.error(this.i18n.getText("failService"));
						});
					})
				}
			}else{
				this.originDataDisplay.list.map(item=>{
					var fileName = item.url.split("/").reverse()[0];
					var desertRef = this.getOwnerComponent()._storageRef.child(fileName);
					
					desertRef.delete().then(function() {
					}).catch(function(error) {
					  return MessageBox.error(this.i18n.getText("failService"));
					});
				})
			}
		},
		
		_updateJmtData: function(jmtData){
	 		if(jmtData.deleteFileList){
				this._deleteFile("updateDoc");
			}
			this.byId("detail").setBusy(true);	
			var updateData = {
				"id" : jmtData.id,
				"data" : {
					"jmtCategory" : jmtData.jmtCategory,
					"jmtCategoryKey" : jmtData.jmtCategoryKey,
					"jmtDate" : jmtData.jmtDate,
					"jmtLocation" : jmtData.jmtLocation,
					"jmtLocationKey" : jmtData.jmtLocationKey,
					"jmtPrice" : jmtData.jmtPrice,
					"jmtRating" : jmtData.jmtRating,
					"jmtRefer" : jmtData.jmtRefer,
					"list" : jmtData.list
				}
			};
			var that = this;
			$.ajax({
				url: "/MapService/api/v1/jmt",
				method : "PUT",	
				headers: {
					"Content-Type": "application/json;charset=utf-8"
				},
				data: JSON.stringify(updateData),
				success: function (data, headers){
					MessageBox.success(that.i18n.getText("successEdit"), {
							actions: [MessageBox.Action.OK],
							emphasizedAction: MessageBox.Action.OK,
							onClose: function (oAction) { 
								if(oAction === "OK"){
									that.getOwnerComponent().reload = true;
									that.byId("fileUploader").setValue();
									var pressedItemModel = that.getView().getModel("pressItem");
									that.originDataDisplay = JSON.parse(JSON.stringify(pressedItemModel.getData()));
									that._changeEditMode(false);
								}
							}
						}
					);
					that.byId("detail").setBusy(false);
				},
				error: function (error){
					that.byId("detail").setBusy(false);	
					return MessageBox.error(that.i18n.getText("failService"));
				}
			});
		},
		
		_deleteJmtData: function(docId){
			var that = this;
			var jmtData = this.getView().getModel("pressItem").getData();
			this.byId("detail").setBusy(true);
			$.ajax({
				url: "/MapService/api/v1/jmt",
				method : "DELETE",
				headers: {
					"Content-Type": "application/json;charset=utf-8"
				},
				data : JSON.stringify({
					"id" : docId
				}),
				success: function(data, headers){
					if(jmtData.list){
						that._deleteFile("deleteDoc");
					}
					MessageBox.success(that.i18n.getText("successDelete"), {
							actions: [MessageBox.Action.OK],
							emphasizedAction: MessageBox.Action.OK,
							onClose: function (oAction) { 
								if(oAction === "OK"){
									that.getOwnerComponent().reload = true;
									that.getRouter().navTo("Main");		
								}
							}
						}
					);
					that.byId("detail").setBusy(false);
				},
				error: function(error){
					that.byId("detail").setBusy(false);
					return MessageBox.warning(that.i18n.getText("failService"));
				}
			});
		}
	});
});