sap.ui.define([
	"./BaseController",
	// "sap/ui/core/mvc/Controller",
	"sap/ui/model/json/JSONModel",
	"sap/m/MessageBox"
], function (BaseController, JSONModel, MessageBox) {
	"use strict";

	return BaseController.extend("istn.ci.edu.kmap.JMT.controller.Main", {

		onInit: function () {
			this.Router = this.getRouter();
			this.getOwnerComponent().reload = true;
			this.i18n = this.getOwnerComponent().getModel("i18n").getResourceBundle();
			this.getRouter().getRoute("Main").attachMatched(this._onRouteMatchedMain, this);	// ~getRoute(name) : "name"의 라우트를 return
		},
		
		_onRouteMatchedMain: function(event){
			if(this.getOwnerComponent().reload){
				this._getJmtList();
				this.getOwnerComponent().reload = false;
			}
		},
		
		onAfterRendering: function (Event){
			this._getMap(this.getView().byId("song_map").getId(), "");
			this._initModels();
			// this._getCategories();
		},

		_initModels: function(){
			this.getView().setModel(new JSONModel(), "searchData");	//검색모델
			this._getJmtList();
			this._getCodeList();
		},
		
		_getCodeList: function(){
			var that = this;
			$.ajax({
				url: "/MapService/api/v1/jmt/codeList",
				method: "GET",
				success: function(data, headers){
					data.categories.push({ code : "ALL", code_text : "전체" });
					data.locations.push({ code : "ALL", code_text : "전체" });
					that.getView().setModel(new JSONModel(data.categories), "Categories");
					that.getView().setModel(new JSONModel(data.locations), "Locations");
				},
				error: function(error){
					MessageBox.warning(that.i18n.getText("failGetCodes"));
				}
			});
		},
		
		_getJmtList: function(){
			var that = this;
			this.byId("listItem").setBusy(true);
			$.ajax({
				url: "/MapService/api/v1/jmt",
				method: "GET",
				success : function(data, headers){
					that.byId("listItem").setBusy(false);
					if(!that.getView().getModel("jmtList")){
						that.getView().setModel(new JSONModel(data.data), "jmtList");
					}else{
						that.getView().getModel("jmtList").setData(data.data);
					}
					if(data.data.length === 0){ that.byId("viewMap").setEnabled(false); }
					that.originData = JSON.stringify(data.data);
				},
				error: function(error){
					that.byId("listItem").setBusy(false);
					MessageBox.warning(that.i18n.getText("failGetJmt"));
				}
			});
		},

		
		onCreate: function(){
			this.getRouter().navTo("Create");
		},
		
		onSearchChange: function(Event){
			var searchData = this.getView().getModel("searchData").getData();
			var jmtData = JSON.parse(this.originData);
			
			if(Event.getSource().getId().split("--").reverse()[0] === "jmtDate"){
				if(!Event.getParameter("valid")){
					Event.getSource().setValueState("Error");
					return Event.getSource().setValueStateText(this.i18n.getText("dateValidator"));
				}else{
					Event.getSource().setValueState("None");	
				}
			}
			
			var filteredData = jmtData.filter(function(date){
				if(searchData.date){
					return date.jmtDate.indexOf(searchData.date) !== -1 ;
				}else{
					return date;
				}
			})
			.filter(function(category){
				if(searchData.category && searchData.category !== "ALL"){
					return category.jmtCategoryKey === searchData.category;
				}else{
					return category;
				}
			})
			.filter(function(location){
				if(searchData.location && searchData.location !== "ALL"){
					return location.jmtLocationKey === searchData.location;
				}else{
					return location;
				}
			});
			
			this.getView().getModel("jmtList").setData(filteredData);
			var viewMap = this.byId("viewMap");
			filteredData.length === 0 ? viewMap.setEnabled(false) : viewMap.setEnabled(true);
		},
		
		
		onViewMap: function(Event){
			this.marker.setMap(null);
			var jmtData = this.getView().getModel("jmtList").getData();
			var points = [];
			jmtData.forEach(function(list){
				points.push(new kakao.maps.LatLng(list.map.y, list.map.x));
			});
			var bounds = new kakao.maps.LatLngBounds();
			var i, marker;
			this.markerArr = [];	//마커삭제로 필요
			for (i = 0; i < points.length; i++) {
			    marker = new kakao.maps.Marker({ position : points[i] }); //마커를 지도에 추가
			    marker.setMap(this.map);
			    this.markerArr.push(marker);
			    
			    bounds.extend(points[i]);
			}
			this.map.setBounds(bounds);
		},
		
		onPressList: function(Event){
			//선택된 아이템의 정보
			var idx = Event.getSource().getBindingContextPath().split("/").reverse()[0];
			var pressedItem = this.getView().getModel("jmtList").getData()[idx];
			
			if(this.byId("switchBtn").getState()){	//detail로
				this.getOwnerComponent().jmtList = JSON.parse(JSON.stringify(pressedItem));
				// this.getRouter().navTo("Display", {
				// 	"idx" : idx
				// });
				this.getRouter().navTo("Display");
			}else{	//지도에 위치 표시
				if(this.markerArr){	//전체지도로 마커여러개일경우 삭제하기
					for(var i in this.markerArr){
						this.markerArr[i].setMap(null);
					}
				}
				var center = new kakao.maps.LatLng(pressedItem.map.y, pressedItem.map.x);
				this.map.setLevel(3);	//지도확대 레벨
				this.map.panTo(center);
			
				if(!this.marker){
					this.marker = new kakao.maps.Marker({
					    position: center //마크가 표시될 좌표 변수
					});
					this.marker.setMap(this.map);
				}else{
					this.marker.setMap(this.map);
					this.marker.setPosition(center);
				}
			}
		}
		
		// _getCategories: function(){
		// 	var that = this;
		// 	// get Category Code list
		// 	$.ajax({
		// 		url: "/MapService/api/v1/category/code",
		// 		method: "GET",
		// 		success : function (data, headers){
		// 			// debugger;
		// 			that.getView().setModel(new JSONModel(data), "categories");
		// 			// that.byId("categoryList").setSelectedKey("ALL");
		// 			if(!that.byId("searchBtn").getEnabled()){
		// 				that.byId("searchBtn").setEnabled(true);
		// 			} 
		// 		},
		// 		error : function (error){
		// 			// debugger;
		// 			MessageBox.warning("코드 리스트를 읽어오지 못했어요:(");
		// 			that.byId("searchBtn").setEnabled(false);
		// 		}
		// 	});
		// },
		
	});
});