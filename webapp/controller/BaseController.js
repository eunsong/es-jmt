sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/core/routing/History",
	"sap/ui/core/UIComponent",
	"sap/ui/model/json/JSONModel",
	"sap/m/MessageBox"
], function(Controller, History, UIComponent, JSONModel, MessageBox) {
	"use strict";

	return Controller.extend("istn.ci.edu.kmap.JMT.controller.BaseController", {


		setJmtModel: function(data){
			var originJmtData = JSON.parse(JSON.stringify(data));
			if(!this.getView().getModel("jmtList")){
				this.getOwnerComponent().setModel(new JSONModel(originJmtData), "jmtList");
			}else{
				this.getView().getModel("jmtList").setData(originJmtData);
			}
		},

		//라우터 인스턴스화
		getRouter : function () { 
			return UIComponent.getRouterFor(this);
		},

		onNavBack: function () {
			var oHistory = History.getInstance();
			var sPreviousHash = oHistory.getPreviousHash();
			
			if (sPreviousHash !== undefined) {
				window.history.go(-1);
			} else {
				this.getRouter().navTo("Main", {}, true);
			}
		},
		
		_getMap: function(id, pressedItem){
			if(!pressedItem){
		        var container = document.getElementById(id); //지도를 담을 HBox의 html Id
		        var options = { //지도를 생성할 때 필요한 기본 옵션
		            center: new kakao.maps.LatLng(37.484177185974836, 127.11674377596071), //지도의 중심좌표.-기본: 회사
		            level: 3 //지도의 레벨(확대, 축소 정도)
		        };
				
		    	this.map = new kakao.maps.Map(container, options); //지도 생성 및 객체 리턴 (컨트롤러 property로 선언)
				this.marker = new kakao.maps.Marker({
				    position: options.center //마크가 표시될 좌표 변수
				});
				this.marker.setMap(null);
				// 마커가 지도 위에 표시되도록 설정합니다
				this.marker.setMap(this.map);
				
			}else{
				var markerPosition = new kakao.maps.LatLng(pressedItem.map.y, pressedItem.map.x)
				this.map.panTo(markerPosition);	// 선택된 아이템의 좌표 정보 기반으로 지도의 중심이동
				this.marker.setPosition(markerPosition); //마크가 표시될 좌표 변수

				this.marker.setMap(this.map);
			}
		},
		
		_getCodeList: function(){
			var that = this;
			$.ajax({
				url: "/MapService/api/v1/jmt/codeList",
				method: "GET",
				success: function(data, headers){
					that.getView().setModel(new JSONModel(data.categories), "Categories");
					that.getView().setModel(new JSONModel(data.locations), "Locations");
				},
				error: function(error){
					MessageBox.warning("카테고리, 지역 리스트를 읽어오지 못했습니다.")
				}
			})
		},
		
		_updateIndicator: function(updated){
		}
		
		// _getData: function(inputData, busyId){
		// 	// get keyword search list
		// 	var bId = busyId;
		// 	var that = this;
		// 	this.byId(bId).setBusy(true);
		// 	$.ajax({
		// 		url: "/MapService/api/v1/place",
		// 		method: "POST",
		// 		headers: {
		// 			"Content-Type": "application/json;charset=utf-8"
		// 		},
		// 		data: JSON.stringify(inputData),
		// 		success : function (data, headers){
		// 			//모델 중복 만들기 안됨
		// 			if(!that.getView().getModel("searchList")){
		// 				that.getView().setModel(new JSONModel(data.data), "searchList");
		// 			}else{
		// 				that.getView().getModel("searchList").setData(data.data);
		// 			}
					
		// 			that.byId(bId).setBusy(false);
					
		// 		},
		// 		error : function (error){
		// 			// debugger;
		// 			that.byId(bId).setBusy(false);
		// 			MessageBox.warning("검색결과를 읽어오지 못했어요:(");
		// 		}
		// 	})
		// }

	});

});